package tech.mastertech.itau.produto.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.produto.dto.Categoria;
import tech.mastertech.itau.produto.dto.Produto;

@Service
public class ProdutoService {

	private List<Produto> produtos;
	private List<Produto> produtosCat;
	private int novoId;

	public ProdutoService() {

		produtos = new ArrayList<>();
		novoId = 1;

	}

	public List<Produto> getProdutos() {

		return produtos;
	}

	public Produto getProduto(int id) {

		for (Produto produto : produtos) {
			if (produto.getId() == id) {
				return produto;
			}

		}

		return null;

	}

	public List<Produto> getProdutos(Categoria categoria) {

		produtosCat = new ArrayList<>();

		for (Produto produto : produtos) {
			if (produto.getCategoria().equals(categoria)) {
				produtosCat.add(produto);
			}

		}

		return produtosCat;

	}

	public Produto atualizaValor(int id, double valor) {

		if (valor <= 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Valor inválido - deve ser positivo");
		}
		
		for (Produto produto : produtos) {
			if (produto.getId() == id) {
				produto.setValor(valor);
				return produto;
			}

		}

		return null;

	}

	public Produto setProduto(Produto produto) {

		produto.setId(novoId);
		novoId++;
		produtos.add(produto);
		return produto;

	}

}
