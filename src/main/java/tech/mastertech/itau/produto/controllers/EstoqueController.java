package tech.mastertech.itau.produto.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.produto.dto.Categoria;
import tech.mastertech.itau.produto.dto.Produto;
import tech.mastertech.itau.produto.services.ProdutoService;

@RestController
@RequestMapping("/estoque")
public class EstoqueController {

	@Autowired
	private ProdutoService produtoService;
	private Produto retorno;
	
	@GetMapping("/produtos")
	public List<Produto> getProdutos(@RequestParam(required=false) Categoria categoria){
		
		if(categoria ==null)
		{
			return produtoService.getProdutos();
		}
		return produtoService.getProdutos(categoria);
	}
	
	@PostMapping("/produto")
	public Produto setProduto(@RequestBody Produto produto) {
		
		return produtoService.setProduto(produto);
		
	}
	
	@GetMapping("/produto/{id}")
	public Produto getProduto(@PathVariable int id) {
		retorno = produtoService.getProduto(id);
		
		if(retorno == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		
		return retorno;
		
	}
	
	@PatchMapping("/produto/{id}")
	public Produto atualizaValor(@PathVariable int id, @RequestBody Map<String, Double> valor) {
		
		
		retorno = produtoService.atualizaValor(id,valor.get("valor"));
		
		if(retorno == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		
		return retorno;
		
	}
	
	
	
	
	
	
	
	
	@GetMapping("/hello")
	public String helloEstoque() {
		return "Hello Estoque!";
	}
	
	@GetMapping("/repetir/{palavra}")
	public String repetir(@PathVariable String palavra) {
		return "<h1> A palavra é " + palavra + "</h1>";
		
	}
	
	@GetMapping("/repetir")
	public String repetirParam(@RequestParam String palavra) {
		
		return "A palavra no header é " + palavra;
		
	}
	
	@PostMapping("/repetir")
	public String repetirBody(@RequestBody String palavra) {
		return "A palavra no body é " + palavra;
	}
	
	

}
